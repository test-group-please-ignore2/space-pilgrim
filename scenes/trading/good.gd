extends Node

# Called when the node enters the scene tree for the first time.
func _ready(id="", amt=1):
	var goodName = ""
	var goodDesc = ""
	var goodVolume = 0
	var goodPrice = 0
	var goodAmt = 0


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func transfer_ownership(new, old):
	"""
	Moves a good from one inventory to another.
	"""
