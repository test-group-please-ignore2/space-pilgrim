extends Node

# Called when the node enters the scene tree for the first time.
func _ready():
	var inventorySize = 0
	var inventoryGoods = []

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func inventory_check():
	"""
	Checks number of items in inventory.
	--------
	Parameters:
		None

	--------
	Returns:
		int
	"""

	var count = 0
	for good in self.inventoryGoods:
		count = count + good.goodAmt

	return count

func inventory_add(good):
	"""
	Adds a good to itself.
	--------
	Parameters:
		good - object
			An object representing a good.

	--------
	Returns:
		None
	"""

	self.inventoryGoods.append(good)

func inventory_remove(good, amt):
	"""
	Removes a good from itself.
	--------
	Parameters:
		good - object 
			An object representing a good.

		amt - int
			The amount of good to remove from the inventory.
	--------
	Returns:
		None
	"""

	for i in range(amt):
		self.inventoryGoods.erase(good)
