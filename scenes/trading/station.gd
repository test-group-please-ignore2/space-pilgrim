extends KinematicBody2D
const Inventory = preload("res://scenes/trading/inventory.tscn")

# Called when the node enters the scene tree for the first time.
func _ready():
	var stationName = ""
	var stationDesc = ""
	var stationSprite = ""
	var stationWeight = 0
	var stationVolume = 0
	var stationInventory = Inventory.new()

# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass

func transferOwnership(new, old):
	self.parent = new.scene

func generateRandomGoods():
	"""
	Debug function to start a station out with random goods.

	--------
	Returns:
		None
	"""

	self.stationInventory.inventory_add()
