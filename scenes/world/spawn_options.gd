extends ItemList
const Trading = preload("res://scenes/trading/_trading.gd")
signal send_spawn(spawn)

var spawnables = {}
var to_spawn = null
var visibility = false

# Called when the node enters the scene tree for the first time.
func _ready():
	
	spawnables.trading = Trading.new()
	
#when parent is ready signal adds items to self
func _on_spawner_ready():
	for group in spawnables:
		for item in spawnables[group].instances:
			var item_desc = group + "|" + item
			self.add_item(item_desc, null, true)

#on selection change set spawnable type from indices
func _on_item_select(index):
	var text = get_item_text(index).split("|")
	var group = text[0]
	var spawnable = text[1]
	
	to_spawn = spawnables[group].instances[spawnable]
	
#button signal spawns selected entities
func _on_spawn():
	if to_spawn == null:
		return
	else:
		emit_signal("send_spawn", to_spawn)
		

func _toggle_visibility():
	visible = visibility
	if visibility == false:
		visibility = true
	else:
		visibility = false

