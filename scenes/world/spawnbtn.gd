extends Button
var scene_to_spawn = null
signal send_spawn(instanced_scene)

# Called when the node enters the scene tree for the first time.
func _ready():
	pass # Replace with function body.

#callback function from pressing button to emit instanced scene to parent
func _send_spawn(spawn):
	emit_signal("send_spawn", spawn)
