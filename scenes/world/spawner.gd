extends Control
signal spawn_instance_of_scene(target_scene)


#triggered by spawn button, gather coordinates and spawn the scene
func _receive_instance_to_spawn(scene):	
	var instanced_scene = scene.instance()
	var x = float(get_node("x_in").text)
	var y = float(get_node("y_in").text)
	
	var pos = Vector2(x, y)
	instanced_scene.position = pos
	spawn_to_parent(instanced_scene)
	

func spawn_to_parent(scene):
	emit_signal("spawn_instance_of_scene", scene)
	print(scene)

