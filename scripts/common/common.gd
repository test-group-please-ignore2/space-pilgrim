""" 
Common library for the reduction of code lines per class.
"""

""" Modify every item in a list with this function : TODO add conditional logic """
func list_f(list: Array, function: FuncRef) :
	for item in list:
		list[item] = function.call(item)
	return list
