const sqlite = preload("res://addons/godot-sqlite/bin/gdsqlite.gdns")
var activedb = sqlite.new()

func main(db="res://db/db", query="", vmode=false):
	if db.to_lower() == "help":
		var helperstring = 'dbpath="res://db/dbname", query="Select * FROM table", vmode=false'
		print(helperstring)
		return helperstring
		
	activedb.path = db
	activedb.verbose_mode = vmode
	activedb.open_db()
	activedb.query(query)
	
	return activedb.query_result
